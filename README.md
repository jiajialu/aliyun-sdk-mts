# aliyun-sdk-mts

#### 项目介绍

阿里云SDK媒体转码管理包重构，支持composer方式使用


#### 安装教程

composer require jiajialu/aliyun-sdk-mts

#### 使用说明

~~~php
<?php
  
require "vendor/autoload.php";
  
use AliCloud\Core\Profile\DefaultProfile;
use AliCloud\Core\DefaultAcsClient;
use AliCloud\Mts\AddMediaWorkflowRequest;
  
//其中一个例子，添加媒体工作流

date_default_timezone_set('PRC');

class AliMts
{
    private $client;
    private $pipelineId = "<pipelineId>";
    private $templateId = "S00000001-100020"; #转码模版ID，m3u8模版，按需配置
    private $ossLocation = "<OssLocation>";
    private $inputBucket = "<mybucket>";
    private $inputPath = "<InputPath>"; #如 "HLS-Encryption"
    private $outputBucket = "<mybucketOut>";
    private $encryptionType = "hls-aes-128";
    private $hlsKeyUri = "<解密密钥的URI>"; #如http://decrypt.testdomain.com
    private $actStart = "Act-Start";
    private $actEncryption = "Act-HLS-Encryption";
    private $actReport = "Act-Report";

    public function __construct($accessKeyId, $accessKeySecret, $regionId = 'cn-shanghai')
    {
        $this->initVodClient($accessKeyId, $accessKeySecret, $regionId);
    }

    /**
     * 初始化客户端
     * @param string $accessKeyId
     * @param string $accessKeySecret
     * @param string $regionId // 服务所在的Region，国内请填cn-shanghai，不要填写别的区域
     * @return DefaultAcsClient
     */
    private function initVodClient($accessKeyId, $accessKeySecret, $regionId)
    {
        $profile = DefaultProfile::getProfile($regionId, $accessKeyId, $accessKeySecret);
        return $this->client = new DefaultAcsClient($profile);
    }

    /**
     * 获取初始化的客户端
     * @return DefaultAcsClient
     */
    public function getAcsClient()
    {
        return $this->client;
    }

    public function addMediaWorkflow($name="HLS加密工作流php") {
        $request = new AddMediaWorkflowRequest();
        $request->setName($name);
        $request->setTopology($this->buildWorkflowTopology());
        $response = $this->client->getAcsResponse($request);
        return $response;
    }


    public function buildWorkflowTopology() {
        $activities = $this->buildActivities();
        $dependencies = $this->buildDependencies();
        $workflow = array(
            "Activities" => $activities,
            "Dependencies" => $dependencies
        );
        echo json_encode($workflow)."\n";
        return json_encode($workflow);
    }

    public function buildActivities() {
        $activities = [
            $this->actStart => $this->buildStartActivity(),
            $this->actEncryption => $this->buildTranscodeActivity(),
            $this->actReport => $this->buildReportActivity()
        ];
        return $activities;
    }

    public function buildStartActivity() {
        $startActivity = array(
            "Name" => $this->actStart,
            "Type" => "Start",
            "Parameters" => $this->buildStartParameters()
        );
        return $startActivity;
    }

    public function buildStartParameters() {
        $startParameters = array(
            "PipelineId" => $this->pipelineId,
            "InputFile" => $this->buildInputFile()
        );
        return $startParameters;
    }

    public function buildInputFile() {
        $inputFile = array(
            "Bucket" => $this->inputBucket,
            "Location" => $this->ossLocation,
            "ObjectPrefix" => $this->inputPath
        );
        return $inputFile;
    }

    public function buildTranscodeActivity() {
        $transcodeParameters = array(
            "Name" => $this->actEncryption,
            "Type" => "Transcode",
            "Parameters" => $this->buildTranscodeParameters()
        );
        return $transcodeParameters;
    }

    public function buildTranscodeParameters() {
        $transcodeParameters = array(
            "OutputBucket" => $this->outputBucket,
            "OutputLocation" => $this->ossLocation,
            "Outputs" => $this->buildOutputsConfig()
        );
        return $transcodeParameters;
    }

    public function buildOutputsConfig() {
        $output = array(
            "ObjectRegex" => $this->actEncryption."/{RunId}/{FileName}",
            "TemplateId" => $this->templateId,
            "Encryption" => $this->buildEncryption()
        );
        $outputs = array($output);
        return $outputs;
    }

    public function buildEncryption() {
        $encryption = array(
            "Type" => $this->encryptionType,
            "KeyUri" => $this->hlsKeyUri
        );
        return $encryption;
    }

    public function buildReportActivity() {
        $reportActivity = array(
            "Name" => $this->actReport,
            "Parameters" => (object)[],
            "Type" => "Report"
        );
        return $reportActivity;
    }

    public function buildDependencies() {
        $subActivityOfStart = array(
            $this->actEncryption
        );
        $subActivityOfTranscode = array(
            $this->actReport
        );
        $dependencies = array(
            $this->actStart => $subActivityOfStart,
            $this->actEncryption => $subActivityOfTranscode,
            $this->actReport => []
        );
        return $dependencies;
    }
}

$mts = new AliMts("<accessKeyId>", "<accessKeySecret>", "<regionId>");
$response = $mts->addMediaWorkflow("我的工作流");
var_dump($response);


~~~

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

